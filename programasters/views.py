import datetime
import logging

import stripe
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.shortcuts import redirect
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics

from programasters.exceptions import UserWithGivenEmailAlreadyExistsError, IncorrectEmailOrPasswordError, \
    CoursePermissionDenied, TokenExpired
from programasters.models import User, Course, Progress, Lesson, Attachment, EmailVerificationToken, Purchase, \
    Token, Product, PasswordChangeToken
from programasters.serializers import CourseSerializer, ProgressSerializer, AttachmentSerializer, UserSerializer, \
    UserWithTokenSerializer
from programasters.utils import send_confirmation_email, get_s3_url, create_payment_session, \
    send_password_recovery_email


class RegistrationView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        email = request.data.get('email', '')
        name = request.data.get('name', '')
        password = request.data.get('password', '')

        if '' in [email, name, password]:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Nieprawidłowe dane'})
        try:
            user = User.objects.create_user(email=email, name=name, password=password)
        except IntegrityError:
            raise UserWithGivenEmailAlreadyExistsError

        email_token = EmailVerificationToken.objects.create(user=user)

        send_confirmation_email(email, email_token.token)

        return Response(status=200, data=UserWithTokenSerializer(user).data)


class LoginView(ObtainAuthToken):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        password = request.data.get('password')

        if None in [email, password]:
            return Response(data={'error': "Oczekiwano 'username' i 'password'"},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise IncorrectEmailOrPasswordError

        if not user.check_password(password):
            raise IncorrectEmailOrPasswordError

        return Response(status=200, data=UserWithTokenSerializer(user).data)


class CourseRetrieveView(generics.RetrieveAPIView):
    permission_classes = [AllowAny]
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class LessonPersonalView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        try:
            lesson_id = kwargs.get('id', 0)
            lesson = Lesson.objects.get(id=lesson_id)
        except Lesson.DoesNotExist:
            return Response(status=400)

        # 2 first modules are for free
        if lesson.module.number > 2 and not Purchase.objects.filter(is_paid=True, user=request.user,
                                                                    product__course=lesson.module.course).exists():
            raise CoursePermissionDenied

        attachments = Attachment.objects.filter(lesson=lesson)
        progress = None
        if request.user:
            p = Progress.objects.filter(user=request.user, lesson=lesson)
            if p.exists():
                progress = ProgressSerializer(p.first()).data

        return Response(status=200, data={
            'url': get_s3_url(f'lessons/{lesson_id}.mp4'),
            'attachments': AttachmentSerializer(attachments, many=True, context={'request': request}).data,
            'progress': progress,
            'name': lesson.name
        })


class CoursePersonalView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            course_id = kwargs.get('id', 0)
            course = Course.objects.get(id=course_id)
        except Course.DoesNotExist:
            return Response(status=400)

        data = {
            'is_bought': Purchase.objects.filter(is_paid=True, user=request.user, product__course=course).exists()
        }
        try:
            last_progress = Progress.objects.filter(lesson__module__course=course).latest('updated_at')
            if last_progress.is_lesson_finished:
                try:
                    next_lesson = Lesson.objects.filter(module__course=course, module=last_progress.lesson.module,
                                                        number__gt=last_progress.lesson.number).first()
                except Lesson.DoesNotExist:
                    try:
                        next_lesson = Lesson.objects.filter(course=course,
                                                            module__number=(last_progress.lesson.module + 1)).first()
                    except ObjectDoesNotExist:
                        next_lesson = None
            else:
                next_lesson = last_progress.lesson
        except Progress.DoesNotExist:
            next_lesson = None

        data['next_lesson'] = None if next_lesson is None else {
            'id': next_lesson.id,
            'module_id': next_lesson.module.id
        }

        return Response(status=200, data=data)


class ProgressUpdateView(APIView):
    def post(self, request, *args, **kwargs):
        lesson = request.data.get('lesson')
        elapsed_seconds = request.data.get('elapsed_seconds')
        is_lesson_finished = request.data.get('is_lesson_finished')

        if None in [lesson, elapsed_seconds, is_lesson_finished]:
            return Response(status=400)

        try:
            progress, _ = Progress.objects.get_or_create(user=request.user, lesson_id=lesson)
        except IntegrityError:
            return Response(status=400)

        progress.elapsed_seconds = elapsed_seconds
        progress.is_lesson_finished = is_lesson_finished
        progress.save()

        return Response(status=200)


class EmailVerificationView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        token = kwargs['token']
        try:
            user = EmailVerificationToken.objects.get(token=token).user
            user.is_verified = True
            user.save()
            return Response(status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class PaymentView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        product_id = request.query_params.get('product', 0)
        course_id = request.query_params.get('course', 0)
        lesson_id = request.query_params.get('lesson', 0)

        user_email = request.query_params.get('email', '')
        try:
            if product_id:
                product = Product.objects.get(id=product_id)
            elif course_id:
                product = Product.objects.filter(course=course_id).last()
            else:
                product = Product.objects.filter(course__modules__lessons__in=[lesson_id]).last()

            user = User.objects.get(email=user_email)
        except ObjectDoesNotExist:
            return Response(status=400)
        try:
            session = create_payment_session(product, user)
        except Exception as e:
            logging.error(e)
            return Response(status=400)

        return redirect(session.url)


class WebhookView(APIView):
    permission_classes = [AllowAny]
    endpoint_secret = 'whsec_d0e843xcP7Nj9MxuDRMhFUX15UVgcTEi'

    def post(self, request, *args, **kwargs):
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, self.endpoint_secret
            )
        except ValueError as e:
            return Response(status=400)
        except stripe.error.SignatureVerificationError as e:
            return Response(status=400)

        if event['type'] == 'checkout.session.completed':
            session = event['data']['object']
            user = User.objects.get(email=session['customer_email'])
            purchase = Purchase.objects.create(user=user, product_id=session['metadata']['product'],
                                               stripe_id=session['id'])

            if session['payment_status'] == 'paid':
                purchase.is_paid = True
                purchase.save()

        elif event['type'] == 'checkout.session.async_payment_failed':
            session = event['data']['object']
            purchase = Purchase.objects.get(stripe_id=session['id'])
            purchase.is_done = True
            purchase.save()

        elif event['type'] == 'checkout.session.async_payment_succeeded':
            session = event['data']['object']
            purchase = Purchase.objects.get(stripe_id=session['id'])
            purchase.is_done = True
            purchase.is_paid = True
            purchase.save()

        return Response(status=200)


class UserDetailsView(APIView):
    def get(self, request, *args, **kwargs):
        return Response(status=200, data=UserSerializer(request.user).data)


class UserResetPasswordView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        token = request.data.get('token')
        password = request.data.get('password')

        try:
            pct = PasswordChangeToken.objects.get(token=token)
        except ObjectDoesNotExist:
            return Response(status=400)
        if pct.created + datetime.timedelta(hours=1) < datetime.datetime.now():
            raise TokenExpired
        user = pct.user
        pct.delete()
        user.set_password(raw_password=password)
        user.save()

        return Response(status=200, data=UserWithTokenSerializer(user).data)


class UserPasswordRecoveryView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')

        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            return Response(status=404)

        pct = PasswordChangeToken.objects.create(user=user)

        send_password_recovery_email(user.email, pct.token)

        return Response(status=200)
