from abc import ABC

from rest_framework import serializers
from programasters.models import User, Module, Course, Lesson, Progress, Attachment, Purchase, Product, Token
from programasters.utils import get_s3_url


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ['id', 'number', 'name']


class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ['id', 'name', 'number', 'lessons']

    lessons = LessonSerializer(many=True)


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'name', 'modules']

    modules = ModuleSerializer(many=True)


class CourseSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'name']


class ProgressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Progress
        fields = ['id', 'elapsed_seconds', 'is_lesson_finished']


class AttachmentSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    class Meta:
        model = Attachment
        fields = ['id', 'name', 'url', 'extension']

    def get_url(self, instance):
        return get_s3_url(f'attachments/{instance.id}.{instance.extension}')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    course = CourseSimpleSerializer()


class PurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        exclude = ['user']

    product = ProductSerializer()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name', 'email', 'purchases']

    purchases = PurchaseSerializer(many=True)


class UserWithTokenSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        Token.objects.filter(user=instance).delete()
        token = Token.objects.create(user=instance)

        return {
            'user': {
                'id': instance.id,
                'name': instance.name,
                'email': instance.email
            },
            'token': token.key
        }
