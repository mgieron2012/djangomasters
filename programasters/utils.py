import logging
import boto3
import smtplib
import stripe

from django.utils.crypto import get_random_string
from botocore.exceptions import ClientError
from django.core.mail import send_mail
from djangoProject1 import settings
from programasters.exceptions import IncorrectEmailError


frontend_url = 'https://nextmasters.vercel.app/'
stripe.api_key = 'sk_test_51Jb67QBtS7H6c0LJs4JOMHBCka9umq6TtnqwecDWy7hOnqIFM2sTvjJKFlrrfKzhH928SemtjVCTH559pN6wJk9S00my3GJ4G2'


def send_confirmation_email(email, token):
    try:
        send_mail('Potwierdź swój email',
                  f'Użyj linku, aby potwierdzić swój email: {frontend_url}confirmEmail?token={token}',
                  'no-reply@programasters.pl', ['mgieron2012@gmail.com'])
    except smtplib.SMTPException as e:
        print(e)
        raise IncorrectEmailError


def send_password_recovery_email(email, token):
    try:
        send_mail('Reset hasła',
                  f'Użyj linku, aby zresetować hasło: {frontend_url}resetPassword?token={token}.'
                  f' Token jest ważny przez godzinę.',
                  'no-reply@programasters.pl', ['mgieron2012@gmail.com'])
    except smtplib.SMTPException as e:
        print(e)
        raise IncorrectEmailError


def get_email_token():
    return get_random_string(30)


def get_auth_token():
    return get_random_string(40)


def get_s3_url(key):
    try:
        return client.generate_presigned_url("get_object", ExpiresIn=settings.s3['ACCESS_EXPIRES_IN'],
                                             Params={'Key': key, 'Bucket': settings.s3['AWS_STORAGE_BUCKET_NAME']})
    except ClientError as e:
        logging.error(e)


client = boto3.client(
    's3',
    'eu-central-1',
    aws_access_key_id=settings.s3['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=settings.s3['AWS_SECRET_ACCESS_KEY']
)


def create_payment_session(product, user):
    return stripe.checkout.Session.create(
        line_items=[
            {
                'amount': product.price,
                'currency': 'PLN',
                'quantity': 1,
                'name': f'Kurs {product.course.name}'
            }
        ],
        payment_method_types=[
            'card',
            'p24',
        ],
        mode='payment',
        success_url=frontend_url + '/account?payment=1',
        cancel_url=frontend_url + '/account?payment=0',
        customer_email=user.email,
        metadata={
            'product': product.id
        }
    )
