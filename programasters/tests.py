from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from rest_framework import status
from programasters.models import User, Course, Module, Lesson, Progress, Product, Purchase, Attachment,\
    EmailVerificationToken


class MyAPITestCase(APITestCase):
    def authorize(self):
        self.client.force_login(self.user)

    def cookieAuthorize(self):
        token = Token.objects.create(user=self.user)
        self.client.cookies.load({'auth_token': token.key})

    def setUp(self):
        self.user = User.objects.create(email="marcin@gmail.com", name="Marcin", password='zaq1@WSX')

    def setUpCourses(self):
        self.courses = [Course.objects.create(name="Python", url="/python"),
                        Course.objects.create(name="Minecraft", url="/minecraft")]

    def setUpModules(self):
        if not hasattr(self, 'courses'):
            self.setUpCourses()

        self.modules = [Module.objects.create(name="Mod 1", number=1, description="Conf", course=self.courses[0]),
                        Module.objects.create(name="Mod 2", number=2, description="Start", course=self.courses[0])]

    def setUpLessons(self):
        if not hasattr(self, 'modules'):
            self.setUpModules()

        self.lessons = [
            Lesson.objects.create(module=self.modules[0], number=1, description="Conf win", video_url="leave_me.mp4"),
            Lesson.objects.create(module=self.modules[0], number=2, description="Conf lin", video_url="/2")]

    def setUpAttachments(self):
        if not hasattr(self, 'lessons'):
            self.setUpLessons()

        self.attachments = [Attachment.objects.create(name="A1", url="bad_name.txt", lesson=self.lessons[0]),
                            Attachment.objects.create(name="A1", url="bad_name.txt", lesson=self.lessons[0])]

    def setUpProgress(self):
        if not hasattr(self, 'lessons'):
            self.setUpLessons()

        self.progress = [Progress.objects.create(lesson=self.lessons[0], elapsed_seconds=60,
                                                 is_lesson_finished=False, user=self.user)]

    def buyCourse(self, course):
        p = Product.objects.create(course=course, level=1, price_gr=100)
        Purchase.objects.create(user=self.user, is_done=True, package=p)


class AccountTest(APITestCase):
    def test_registration(self):
        data = {
            'name': 'Marcin',
            'password': "secret_password",
            'email': 'marcin@gmail.com'
        }
        response = self.client.post('/api/register/', data=data)
        self.assertIn('user', response.data)
        self.assertIn('token', response.data)

    def test_registration_same_email(self):
        data = {
            'name': 'Marcin',
            'password': "secret_password",
            'email': 'marcin@gmail.com'
        }
        self.client.post('/api/register/', data=data)
        response = self.client.post('/api/register/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('error', response.data)

    def test_registration_bad_data(self):
        data = {
            'no_name': '',
            'password': "secret_password",
            'email': 'marcin@gmail.com'
        }
        response = self.client.post('/api/register/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('error', response.data)

    def test_login(self):
        data = {'username': "marcin@gmail.com", 'password': 'zaq1@WSX'}
        user = User.objects.create_user(email=data['username'], name="Marcin", password=data['password'])
        user.save()
        response = self.client.post('/api/login/', data=data)
        self.assertEqual(user.pk, response.data['user']['id'])
        self.assertEqual(user.name, response.data['user']['name'])
        self.assertIn('token', response.data)

    def test_login_bad_password(self):
        data = {'username': "marcin@gmail.com", 'password': 'zaq1@WSX'}
        user = User.objects.create_user(email=data['username'], name="Marcin", password="ZAQ!2wsx")
        user.save()
        response = self.client.post('/api/login/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], "Niepoprawny email lub hasło")

    def test_login_bad_data(self):
        data = {'email': "marcin@gmail.com", 'password': 'zaq1@WSX'}
        user = User.objects.create_user(email=data['email'], name="Marcin", password=data['password'])
        user.save()
        response = self.client.post('/api/login/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], "Oczekiwano 'username' i 'password'")

    def test_update_password(self):
        user = User.objects.create_user(email="marcin@gmail.com", name="Marcin", password='zaq1@WSX')
        user.save()


class CourseRetrieveTest(MyAPITestCase):
    def test_retrieve(self):
        self.setUpLessons()
        self.authorize()

        response = self.client.get(f'/api/courses/{self.courses[0].pk}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = response.data
        self.assertEqual(data['name'], self.courses[0].name)
        modules = data['modules']
        self.assertEqual(modules[0]['number'], self.modules[0].number)
        self.assertEqual(modules[1]['description'], self.modules[1].description)
        lessons = modules[0]['lessons']
        self.assertEqual(lessons[1]['description'], self.lessons[1].description)


class ProgressRetrieveUpdateViewTest(MyAPITestCase):
    def test_retrieve(self):
        self.setUpProgress()
        self.authorize()
        response = self.client.get(f'/api/progress/{self.courses[0].id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        p = self.progress[0]
        self.assertEqual(p.elapsed_seconds, data['elapsed_seconds'])
        self.assertEqual(p.lesson.number, data['lesson_number'])
        self.assertEqual(p.lesson.module.number, data['module_number'])

    def test_update(self):
        self.authorize()
        self.setUpProgress()
        request_data = {
            'elapsed_seconds': 100,
            'is_lesson_finished': False,
            'module_number': self.progress[0].lesson.module.number,
            'lesson_number': self.progress[0].lesson.number
        }
        response = self.client.put(f'/api/progress/{self.courses[0].id}/', data=request_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        p = Progress.objects.get(id=self.progress[0].id)
        self.assertEqual(p.elapsed_seconds, 100)
        self.assertEqual(p.lesson.number, self.progress[0].lesson.number)

    def test_create(self):
        self.setUpLessons()
        self.authorize()

        request_data = {
            'elapsed_seconds': 100,
            'is_lesson_finished': False,
            'module_number': self.lessons[0].module.number,
            'lesson_number': self.lessons[0].number
        }
        response = self.client.put(f'/api/progress/{self.courses[0].id}/', data=request_data)

        p = Progress.objects.get(user=self.user)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(p.elapsed_seconds, 100)
        self.assertEqual(p.lesson.number, self.lessons[0].number)

    def test_bad_data(self):
        self.setUpProgress()
        self.authorize()
        request_data = {
            'elapsed_seconds': 100,
            'is_lesson_finished': False,
            'module_number': 759,
            'lesson_number': 201358
        }
        response = self.client.put(f'/api/progress/{self.courses[0].id}/', data=request_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PermissionTest(MyAPITestCase):
    def test_courses_owner_ok(self):
        self.setUpLessons()
        self.buyCourse(self.courses[0])
        self.cookieAuthorize()

        response = self.client.get(f'/api/video/{self.courses[0].pk}/{self.lessons[0].pk}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_courses_owner_no_perm(self):
        self.setUpLessons()
        self.cookieAuthorize()

        response = self.client.get(f'/api/video/{self.courses[0].pk}/{self.lessons[0].pk}/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class AttachmentTest(MyAPITestCase):
    def test_list_attachments(self):
        self.setUpAttachments()
        self.authorize()

        response = self.client.get(f'/api/attachments/{self.lessons[0].pk}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data[0]['name'], self.attachments[0].name)
        self.assertEqual(data[1]['url'], self.attachments[1].url)

    def test_get_attachment_forbidden(self):
        self.setUpAttachments()
        self.cookieAuthorize()
        response = self.client.get(f'/api/attachment/{self.courses[0].pk}/leave_me.test/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_attachment_ok(self):
        self.setUpAttachments()
        self.buyCourse(self.courses[0])
        self.cookieAuthorize()
        response = self.client.get(f'/api/attachment/{self.courses[0].pk}/leave_me.test/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class EmailVerificationTest(MyAPITestCase):
    def test_verify(self):
        verification_token = EmailVerificationToken.objects.create(user=self.user).token
        self.assertFalse(self.user.is_verified)
        response = self.client.get(f'/api/email/verify/{verification_token}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.is_verified)

    def test_verify_wrong_code(self):
        response = self.client.get(f'/api/email/verify/some_random_characters_123/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_verify_random_token_generation(self):
        EmailVerificationToken.objects.create(user=self.user)
        EmailVerificationToken.objects.create(user=self.user)
        tokens = EmailVerificationToken.objects.all()
        self.assertNotEquals(tokens[0].token, tokens[1].token)
        self.assertEqual(len(tokens[0].token), 30)
        self.assertEqual(len(tokens[1].token), 30)
