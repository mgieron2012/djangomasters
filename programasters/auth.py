import datetime
from rest_framework.exceptions import APIException
from djangoProject1 import settings
from programasters.models import User, Token
from rest_framework import authentication


class TokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            key = retrieve_token_from_request(request)
        except (TypeError, AttributeError, IndexError):
            return None, None
        if settings.DEBUG and key == "TEST":
            return User.objects.get_or_create(email="test", defaults={'name': 'TEST'})
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise IncorrectTokenException
        if token.updated_at < datetime.datetime.now() - datetime.timedelta(hours=10):
            raise TokenExpiredException
        token.save()
        return token.user, None


def retrieve_token_from_request(request):
    return request.META.get('HTTP_AUTHORIZATION').split()[1]


class IncorrectTokenException(APIException):
    status_code = 460
    default_detail = 'Incorrect token.'


class TokenExpiredException(APIException):
    status_code = 461
    default_detail = 'Token expired.'
