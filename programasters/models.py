from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models

from programasters.utils import get_email_token, get_auth_token


class UserManager(BaseUserManager):
    def create_user(self, email, name, password):
        user = self.model(
            email=email,
            name=name,
            is_admin=False
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        if not email:
            raise ValueError('User must have email')

        user = self.model(
            email=email,
            name=name,
            is_admin=True
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    email = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=100)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'password']

    objects = UserManager()

    def __str__(self):
        return self.get_username()


class Token(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)
    key = models.CharField(max_length=40, default=get_auth_token)


class EmailVerificationToken(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=30, default=get_email_token)


class PasswordChangeToken(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=40, default=get_auth_token)
    created = models.DateTimeField(auto_now_add=True)


class Course(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)


class Module(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name="modules")
    number = models.IntegerField()
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ['number']


class Lesson(models.Model):
    id = models.AutoField(primary_key=True)
    module = models.ForeignKey(
        Module, on_delete=models.CASCADE, related_name="lessons")
    number = models.IntegerField()
    name = models.CharField(max_length=1000)

    class Meta:
        ordering = ['number']
        unique_together = ['module', 'number']


class Progress(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    elapsed_seconds = models.IntegerField(default=0)
    is_lesson_finished = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    price = models.IntegerField()


class Purchase(models.Model):
    id = models.AutoField(primary_key=True)
    stripe_id = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="purchases")
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    is_done = models.BooleanField(default=False)
    is_paid = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)


class Attachment(models.Model):
    id = models.AutoField(primary_key=True)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    extension = models.CharField(max_length=10, default="txt")
