from rest_framework.exceptions import APIException


class UserWithGivenEmailAlreadyExistsError(APIException):
    status_code = 450
    default_detail = "User with given email already exists."


class IncorrectEmailError(APIException):
    status_code = 451
    default_detail = "Incorrect email."


class IncorrectEmailOrPasswordError(APIException):
    status_code = 452
    default_detail = "Incorrect email or password."


class TokenExpired(APIException):
    status_code = 453
    default_detail = "Auth token expired"


class CoursePermissionDenied(APIException):
    status_code = 454
    default_detail = "Course permission denied"
