from django.urls import path
from programasters.views import LoginView, RegistrationView, CourseRetrieveView, EmailVerificationView, \
    CoursePersonalView, ProgressUpdateView, LessonPersonalView, PaymentView, WebhookView, UserDetailsView, \
    UserResetPasswordView, UserPasswordRecoveryView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('register/', RegistrationView.as_view()),
    path('email/verify/<str:token>/', EmailVerificationView.as_view()),
    path('progress/', ProgressUpdateView.as_view()),
    path('courses/<int:pk>/', CourseRetrieveView.as_view()),
    path('courses/private/<int:id>/', CoursePersonalView.as_view()),
    path('lessons/private/<int:id>/', LessonPersonalView.as_view()),
    path('pay/', PaymentView.as_view()),
    path('stripe-webhooks/', WebhookView.as_view()),
    path('user/', UserDetailsView.as_view()),
    path('password/reset/', UserResetPasswordView.as_view()),
    path('password/recovery/', UserPasswordRecoveryView.as_view()),
]
